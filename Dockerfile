FROM python:3

ENV HOME=/opt/app-root/

WORKDIR ${HOME}

COPY . .

RUN pip install -r requirements.txt

CMD [ "python", "hello.py" ]